#!/bin/bash

#curr_player=firefox

for player in $(playerctl -l); do
	if [[ $(playerctl metadata --player=${player} --format "{{lc(status)}}") == "playing" ]]; then
		curr_player=$player
		break
    else
        curr_player=$player
	fi
done
#echo $curr_player
class=$(playerctl metadata --player=${curr_player} --format '{{lc(status)}}')
if [[ $curr_player == spotify ]]; then
	icon=""
elif [[ $curr_player == firefox* ]]; then
	icon=""
else
    icon=" "
fi

info_artist=$(playerctl metadata --player=${curr_player} --format '{{artist}}')
info_title=$(playerctl metadata --player=${curr_player} --format '{{title}}')

if [[ ${#info_title} -gt 15 ]] && [[ $info_artist == "" ]]; then
	info_title=$(echo $info_title | cut -c1-24)"..."
elif [[ ${#info_title} -gt 15 ]] && [[ ${#info_artist} -gt 0 ]]; then
#else
	info_title=$(echo $info_title | cut -c1-12)"..."
fi
if [[ ${#info_artist} -gt 15 ]]; then
    info_artist=$(echo $info_artist | cut -c1-12)"..."
fi

if [[ $info_artist == "" ]]; then
    info=$info_title
else
    info=$info_title" - "$info_artist
fi
if [[ $class == "playing" ]]; then
	text=$icon" "$info"  "
elif [[ $class == "paused" ]]; then
	text=$icon" "$info"  "
elif [[ $class == "stopped" ]]; then
	text=$icon"  "
else
    text=$icon
fi

echo -e "{\"text\":\" "$text"\", \"class\":\""$class"\"}"
