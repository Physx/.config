#zsh_aliases

alias ll='ls -lah --color'
alias ls='ls --color'
alias pac='sudo pacman'
alias py='python3'
alias cp='rsync --info=progress2'

alias srm='sudo rm -rf'

#alias intruder='ssh r.romany@icg2156.icg.kfa-juelich.de'
#alias v='nvim'
# openconnect vpn to FZ Juelich
alias fz_vpn='sudo openconnect -k PKCS12 -c ~/private/Zertifikate/Roman_Romany_2020-04-09.p12 wingate.zam.kfa-juelich.de'

# connect to Z Laufwerk
alias zhd='sudo mount.cifs //iek8-w2k8.kfa-juelich.de/dfsroot /mnt/z/ -o username=r.romany domain=iek8-w2k8'


alias a='fasd -a'        # any
alias s='fasd -si'       # show / search / select
alias d='fasd -d'        # directory
alias f='fasd -f'        # file
alias sd='fasd -sid'     # interactive directory selection
alias sf='fasd -sif'     # interactive file selection
alias z='fasd_cd -d'     # cd, same functionality as j in autojump
alias zz='fasd_cd -d -i' # cd with interactive selection

alias v='f -e nvim' # quick opening files with vim
alias m='f -e mplayer' # quick opening files with mplayer
alias o='a -e xdg-open' # quick opening files with xdg-open
