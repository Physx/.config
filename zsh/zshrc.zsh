# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort access
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list 'r:|[._-]=** r:|=**' 'm:{[:lower:]}={[:upper:]}' 'l:|=* r:|=*' '+'
zstyle ':completion:*' max-errors 3 numeric
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' squeeze-slashes true
zstyle :compinstall filename '/home/xxx/.zshrc'

autoload -Uz compinit
compinit

#prompt
autoload -U colors && colors

# minimal zsh
source $HOME/.config/zsh/minimal/minimal.zsh


# Adding digestif to Path
#
export PATH="$HOME/.luarocks/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

export PATH="$HOME/apps/nvim:$PATH"
export PATH="$HOME/bin/:$PATH"

export TERM=xterm


module () { 
    eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
}

ml () { 
    eval $($LMOD_DIR/ml_cmd "$@")
}

jutil () { 
    if [ $# -ge 1 ] && [ "$1" = "env" ]; then
        eval $($JUMO_USRCMD_EXEC $@);
    else
        $JUMO_USRCMD_EXEC $@;
    fi
}

# direnv
#eval "$(direnv hook zsh)"
#
#fasd
eval "$(fasd --init auto)"
