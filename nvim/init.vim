set number
set relativenumber
syntax on

"tabstop
set tabstop=4
set expandtab
set shiftwidth=4


set noshowmode
set mouse=a

" set map leader
let mapleader = "\<Space>"

" autocmd vimenter * NERDTree
" Shortcuts
map <C-n> :NERDTreeToggle<CR>
map <C-f> :NERDTreeFocus<CRa>
nnoremap <F3> :set hlsearch!<CR>
nnoremap <Tab> :bn<CR>
nnoremap <S-Tab> :bp<CR>
" save
map <C-s> :w<CR>
" quit
map <C-q> :q<CR>

" automatic installation
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-sensible'
 "Plug 'vim-airline/vim-airline'
 "Plug 'vim-airline/vim-airline-themes'
"Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'

Plug 'Xuyuanp/nerdtree-git-plugin'

" Conquer of Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'neoclide/'
" Tex
Plug 'lervag/vimtex'

" Markdown Rendering
"Plug 'iamcco/mathjax-support-for-mkdp'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }

" ultsnips
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

call plug#end()

" coc Completion Experience
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" vimtex automatic update mupdf
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_latexmk_engines = {'lualatex': '-lualatex'}
let g:tex_flavor = 'plain'
"let g:vimtex_compiler_engine = 'lualatex'

"let g:mkdp_auto_start = 1

let g:coc_global_extensions=['coc-css', 'coc-vimtex', 'coc-java', 'coc-json', 'coc-ultisnips']

let g:python3_host_prog = '/bin/python3.8'

set termguicolors

" let g:airline_theme='dracula'
set background=dark
let g:one_allow_italics = 1
colorscheme one

let g:lightline = {'colorscheme': 'one'}
" let g:airline_theme='deus'
" white Space after Comment 
let g:NERDTrimTrailingWhitespace = 1
