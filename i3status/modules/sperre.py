import sys
import os
import pickle
from PIL import Image
import numpy as np

# radius
r = 4
# diameter
d = 2*r
# path to maskfiles
mask_path = ".lockscreenmasks"


def load_mask(radius, img_size):
    """
    Try to load an existing mask and check the size.

    parameters:
        radius (int): Radius of the circles.
        img_size (tuple): Pixels in y- and x-direction

    returns:
        mask or None
    """
    mask_dump_file_name = os.path.join(mask_path, "mask{}".format(radius))
    mask = None
    if os.path.isfile(mask_dump_file_name):
        with open(mask_dump_file_name, "rb") as f:
            mask = pickle.load(f)
        if mask.shape != img_size:
            print("existing mask has not the correct size")
            mask = None
    return mask


def create_mask(radius, img_size):
    """
    Creates a mask and saves it for future calls.

    parameters:
        radius (int): Radius of the circles.
        img_size (tuple): Pixels in y- and x-direction

    returns:
        mask
    """
    r = radius
    d = 2*r
    mask = np.zeros(img_size, dtype="uint8")
    for y in range(r):
        for x in range(r):
            # set mask = 1 in the circles in every quarter
            if np.sqrt((x-r)**2 + (y-r)**2) < r:
                mask[y::d, x::d] = 1
                mask[y::d, d-x-1::d] = 1
                mask[d-y-1::d, x::d] = 1
                mask[d-y-1::d, d-x-1::d] = 1
    mask_dump_file_name = os.path.join(mask_path, "mask{}".format(radius))
    with open(mask_dump_file_name, "wb") as f:
        pickle.dump(mask, f)
    return mask


def get_mask(radius, img_size):
    """
    Load or create a mask.

    parameters:
        radius (int): Radius of the circles.
        img_size (tuple): Pixels in y- and x-direction

    returns:
        mask
    """
    mask = load_mask(radius, img_size)
    if mask is None:
        print("could not load mask, creating new mask")
        mask = create_mask(radius, img_size)
    return mask

img_name = sys.argv[1]
img = Image.open(img_name)

if not os.path.isdir(mask_path):
    os.makedirs(mask_path)

size_pixel = img.size[0] // d, img.size[1] // d
img_pixel = img.resize(size_pixel).resize((size_pixel[0]*d, size_pixel[1]*d))

img_array = np.array(img_pixel)

mask = get_mask(r, img_array.shape)
img_array *= mask

img_circles = Image.fromarray(img_array).resize(img.size)
img_circles.save(img_name)
