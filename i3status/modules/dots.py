#!/usr/bin/env python3

import sys
from PIL import Image
import numpy as np

img_name = sys.argv[1]
img = Image.open(img_name)

# r
r = 4
# diameter
d = 2*r

size_pixel = img.size[0] // d, img.size[1] // d
img_pixel = img.resize(size_pixel).resize((size_pixel[0]*d, size_pixel[1]*d))

img_array = np.array(img_pixel)

mask = np.zeros((d, d), dtype="uint8")
for y in range(r):
    for x in range(r):
        if np.sqrt((x-r)**2 + (y-r)**2) < r:
            mask[y, x] = 1
            mask[y, d-x-1] = 1
            mask[d-y-1, x] = 1
            mask[d-y-1, d-x-1] = 1

#for y in range(img_array.shape[0]):
#    for x in range(img_array.shape[1]):
#        img_array[y, x] *= mask[y%d, x%d]
for y in range(d):
    for x in range(d):
        img_array[y::d, x::d] *= mask[y, x]

img_circles = Image.fromarray(img_array).resize(img.size)
img_circles.save(img_name)
